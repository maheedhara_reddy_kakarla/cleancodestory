﻿using System;

namespace Naming.Task3
{
    public class HarsahdNumber
    {
        // print some Harshad numbers
        private static void Main(string[] args)
        {
            var sequenceLimit = 1000; 
            for (int i = 1; i <= sequenceLimit; i++)
            {
                if (IsHarshadNumber(i))
                {
                    Console.WriteLine(i);
                }
            }

            Console.Write("Press key...");
            Console.ReadKey();
        }

        private static bool IsHarshadNumber(int i)
        {
            return i % SumOfIndividualDigitsOf(i) == 0;
        }

        private static int SumOfIndividualDigitsOf(int number)
        {
            int sum = 0;
            while (number != 0)
            {
                sum += number % 10;
                number = number / 10;
            }
            return sum;
        }
    }
}
