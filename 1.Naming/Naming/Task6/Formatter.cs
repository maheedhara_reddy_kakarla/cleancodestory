﻿using System;
using System.Linq;

namespace Naming.Task6
{
    public static class Formatter
    {
        private const string HeaderDelimiter = "+";
        private const string Pipe = "|";
        private const string Minus = "-";
        private const string Underscore = " _ ";

        public static void Main(string[] args)
        {
            Console.WriteLine(FormatKeyValuePair("enable", "true"));
            Console.WriteLine(FormatKeyValuePair("name", "Bob"));

            Console.Write("Press key...");
            Console.ReadKey();
        }

        private static string FormatKeyValuePair(string key, string value)
        {
            string content = key + Underscore + value;
            string minuses = Repeat(Minus, content.Length);
            return HeaderDelimiter + minuses + HeaderDelimiter + "\n" +
                   Pipe + content + Pipe + "\n" +
                   HeaderDelimiter + minuses + HeaderDelimiter + "\n";
        }

        private static string Repeat(string symbol, int times)
        {
            return string.Join(string.Empty, Enumerable.Repeat(symbol, times));
        }
    }
}
