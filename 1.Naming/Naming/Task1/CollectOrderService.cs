﻿using Naming.Task1.ThirdParty;

namespace Naming.Task1
{
    public class CollectOrderService : IOrderService
    {
        private readonly ICollectionService _collectionService;
        private readonly INotificationManager _notificationManager;

        private const int Info_Notification_Level = 4;
        private const int Critical_Notification_Level = 1;


        public CollectOrderService(ICollectionService collectionService, INotificationManager notificationManager)
        {
            _collectionService = collectionService;
            _notificationManager = notificationManager;
        }

        public void SubmitOrder(IOrder order)
        {
            if (_collectionService.IsEligibleForCollect(order))
            {
                _notificationManager.NotifyCustomer(Message.ReadyForCollect, Info_Notification_Level);
            }
            else
            {
                _notificationManager.NotifyCustomer(Message.ImpossibleToCollect, Critical_Notification_Level); 
            }
        }
    }
}
