﻿using System.Linq;
using Naming.Task5.ThirdParty;

namespace Naming.Task5
{
    public class FileExtensionPredicate : IPredicate<string>
    {
        private readonly string[] _fileExtensions;

        public FileExtensionPredicate(string[] extensions)
        {
            this._fileExtensions = extensions;
        }

        public bool Test(string fileName)
        {
            fileName = fileName.ToLower();
            return _fileExtensions.Any(fileName.EndsWith);
        }
    }
}
