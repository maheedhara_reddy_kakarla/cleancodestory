﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.FileProviders;
using Naming.Task5.ThirdParty;

namespace Naming.Task5
{
    public class FileManager
    {
        private static readonly string[] imageExtensions = {"jpg", "png"};
        private static readonly string[] documentExtentions = {"pdf", "doc"};

        private const string BaseNameSpace = "Naming.Resources";
        private readonly IFileProvider fileProvider = new EmbeddedFileProvider(Assembly.GetExecutingAssembly(), BaseNameSpace);

        public IFileInfo RetrieveFile(string fileName)
        {
            ValidateFileType(fileName);
            return fileProvider.GetFileInfo(fileName);
        }

        #region RetrieveFile

        private static void ValidateFileType(string fileName)
        {
            if (IsInvalidFileType(fileName))
            {
                throw new InvalidFileTypeException();
            }
        }

        private static bool IsInvalidFileType(string fileName)
        {
            return IsInvalidImage(fileName) && IsInvalidDocument(fileName);
        }

        private static bool IsInvalidImage(string fileName)
        {
            var imageExtensionsPredicate = new FileExtensionPredicate(imageExtensions);
            return !imageExtensionsPredicate.Test(fileName);
        }

        private static bool IsInvalidDocument(string fileName)
        {
            var documentExtensionsPredicate = new FileExtensionPredicate(documentExtentions);
            return !documentExtensionsPredicate.Test(fileName);
        }

        #endregion

        public List<string> GetAllImages()
        {
            return GetFilesByExtensions(imageExtensions);
        }

        public List<string> GetAllDocuments()
        {
            return GetFilesByExtensions(documentExtentions);
        }

        private List<string> GetFilesByExtensions(string[] allowedExtensions)
        {
            var predicate = new FileExtensionPredicate(allowedExtensions);
            return fileProvider.GetDirectoryContents(string.Empty)
                .Select(f => f.Name)
                .Where(predicate.Test)
                .ToList();
        }
    }
}
