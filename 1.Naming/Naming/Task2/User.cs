﻿using System;

namespace Naming.Task2
{
    public class User
    {
        private DateTime DateOfBirth;

        private string Name;

        private bool IsAdmin;

        private User[] Subordinates;

        private int Rating;

        public User(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            var subordinates = string.Join<User>(", ", Subordinates);
            return $"User [Date Of Birth={DateOfBirth};Name={Name};Is Admin ={IsAdmin};Subordinates={subordinates};Rating ={Rating}]";
        }
    }
}
