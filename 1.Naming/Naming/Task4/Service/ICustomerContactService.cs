﻿using Naming.Task4.ThirdParty;

namespace Naming.Task4.Service
{
    public interface ICustomerContactService
    {
        CustomerContact GetCustomerContactDetailsByCustomerId(float customerId);

        void UpdateCustomerContactDetails(CustomerContact customerContact);
    }
}
