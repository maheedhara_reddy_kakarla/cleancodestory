﻿using System;
using Comments.Task1.ThirdParty;

namespace Comments.Task1
{
    public static class MortgageInstallmentCalculator
    {
        public static double CalculateMonthlyPayment(int principal, int termInYears, double annualInterestRate)
        {
            EnsureValidValues(principal, termInYears, annualInterestRate);
            int termInMonths = termInYears * 12;
            if (annualInterestRate == 0)
            {
                return (double)principal / termInMonths;
            }
            var monthlyPayment = Calculate(principal, annualInterestRate, termInMonths);
            return monthlyPayment;
        }

        private static double Calculate(int principal, double annualInterestRate, int termInMonths)
        {
            var annualInterestAmount = annualInterestRate / 100;
            double monthlyInterestAmount = annualInterestAmount / 12;
            double monthlyPayment = principal * monthlyInterestAmount / (1 - Math.Pow(1 + monthlyInterestAmount, -termInMonths));
            return monthlyPayment;
        }

        private static void EnsureValidValues(int principal, int termInYears, double annualInterestRate)
        {
            if (principal < 0 || termInYears <= 0 || annualInterestRate < 0)
            {
                throw new InvalidInputException("Negative values are not allowed");
            }
        }
    }
}
