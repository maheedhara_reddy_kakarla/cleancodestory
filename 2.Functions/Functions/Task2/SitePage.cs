﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Functions.Task2
{
    public class SitePage
    {
        private const string Protocol = "http://";
        private const string EditableParameter = "/?edit=true";
        private const string Domain = "mysite.com";

        public string SiteGroup { get; }
        public string UserGroup { get; }

        public SitePage(string siteGroup, string userGroup)
        {
            SiteGroup = siteGroup;
            UserGroup = userGroup;
        }

        public string GetEditablePageUrl(IDictionary<string, string> parameters)
        {
            var urlBuilder = new StringBuilder();
            urlBuilder.Append(GetBaseUrl());
            urlBuilder.Append(EditableParameter);
            urlBuilder.Append(ConvertToQueryString(parameters));
            urlBuilder.Append(GetAttributes());
            return urlBuilder.ToString();
        }

       
        private string GetBaseUrl()
        {
            return Protocol + Domain;
        }


        private string ConvertToQueryString(IDictionary<string, string> parameters)
        {
            var paramsString = new StringBuilder();
            foreach (var parameter in parameters)
            {
                paramsString.Append($"&{parameter.Key}={parameter.Value}");
            }

            return paramsString.ToString();
        }

        private string GetAttributes()
        {
            return $"&siteGrp={SiteGroup}&userGrp={UserGroup}";
        }

    }
}