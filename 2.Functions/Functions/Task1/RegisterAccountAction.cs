﻿using Functions.Task1.ThirdParty;
using System;
using System.Collections.Generic;

namespace Functions.Task1
{
    public class Account
    {
        public IPasswordChecker PasswordChecker { get; set; }
        public IAccountManager AccountManager { get; set; }

        // Minimum Name should be specified.
        private static readonly int MinimumAccountNameLenth = 5;
        private static readonly int MinimumPasswordLenth = 8;

        public void Register(IAccount account)
        {
            ValidateAccount(account);
            SetInitialAccountData(account);
            CreateAccount(account);
        }

        private void ValidateAccount(IAccount account)
        {
            ValidateUserName(account);
            ValidatePassword(account);
        }

        private void ValidatePassword(IAccount account)
        {
            string password = account.GetPassword();
            if (password.Length <= MinimumPasswordLenth)
            {
                if (PasswordChecker.Validate(password) != CheckStatus.Ok)
                {
                    throw new WrongPasswordException();
                }
            }
        }

        private static void ValidateUserName(IAccount account)
        {
            if (account.GetName().Length <= MinimumAccountNameLenth)
            {
                throw new WrongAccountNameException();
            }
        }

        private static void SetInitialAccountData(IAccount account)
        {
            account.SetCreatedDate(new DateTime());
            account.SetAddresses(GetAddressesFromAccount(account));
        }

        private static List<IAddress> GetAddressesFromAccount(IAccount account)
        {
            return new List<IAddress>
            {
                account.GetHomeAddress(),
                account.GetWorkAddress(),
                account.GetAdditionalAddress()
            };
        }

        private void CreateAccount(IAccount account)
        {
            AccountManager.CreateNewAccount(account);
        }


    }
}