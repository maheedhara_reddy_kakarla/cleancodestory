﻿using System.Collections.Generic;
using System.Linq;
using Functions.Task4.ThirdParty;

namespace Functions.Task4
{
    public class Order
    {
        public IList<IProduct> Products { get; set; }

        public double GetTotalPriceOfAvailableProducts()
        {
            var totalPriceOfAllAvailableProducts = Products.Where(p => p.IsAvailable()).Sum(p => p.GetProductPrice());
            return totalPriceOfAllAvailableProducts;
        }
    }
}
