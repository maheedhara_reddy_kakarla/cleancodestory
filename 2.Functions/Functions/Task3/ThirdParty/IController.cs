﻿namespace Functions.Task3.ThirdParty
{
    public interface IController
    {
        void HandleAuthenticationSuccess(string user);

        void HandleAuthenticationFailure();
    }
}