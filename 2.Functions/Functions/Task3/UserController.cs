﻿using Functions.Task3.ThirdParty;

namespace Functions.Task3
{
    public abstract class UserController : IController
    {
        private readonly UserAuthenticator userAuthenticator;

        protected UserController(UserAuthenticator userAuthenticator)
        {
            this.userAuthenticator = userAuthenticator;
        }

        public void AuthenticateUser(string userName, string password)
        {
            if(Authenticate(userName, password))
            {
                HandleAuthenticationSuccess(userName);
                return;
            }
            HandleAuthenticationFailure();
        }

        private bool Authenticate(string userName, string password)
        {
            var user = userAuthenticator.Login(userName, password);
            return user != null; 
        }

        public abstract void HandleAuthenticationSuccess(string user);
        public abstract void HandleAuthenticationFailure();
    }
}