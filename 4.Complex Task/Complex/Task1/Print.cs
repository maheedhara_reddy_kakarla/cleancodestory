﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Complex.Task1.ThirdParty;

namespace Complex.Task1
{
    public class Print : ICommand
    {
        private readonly IDatabaseManager manager;
        private readonly IView view;
        private string tableName;
        private int _maximumColumnSize;
        private int _numberOfColumns;
        private string _defaultColumnValue;



        public Print(IView view, IDatabaseManager manager)
        {
            this.view = view;
            this.manager = manager;
        }

        public bool CanProcess(string command)
        {
            return command.StartsWith("print ");
        }

        public void Process(string command)
        {
            ExtractTableNameFromCommand(command);
            var dataSets = manager.GetTableData(tableName);
            SetDisplayParameters(dataSets);
            view.Write(GetFormattedTableWithData(dataSets));
        }

        private void ExtractTableNameFromCommand(string command)
        {
            var commandPart = command.Split(' ');
            if (commandPart.Length != 2)
                throw new ArgumentException(
                    "incorrect number of parameters. Expected 1, but is " + (commandPart.Length - 1));
            tableName = commandPart[1];
        }

        private void SetDisplayParameters(IList<IDataSet> dataSets)
        {
            _maximumColumnSize = GetMaximumColumnSizeForDisplay(dataSets);
            _numberOfColumns = GetColumnCount(dataSets);
            _defaultColumnValue = new string(' ', _maximumColumnSize);
        }

        private string GetFormattedTableWithData(IList<IDataSet> dataSets)
        {
            if (dataSets.Count <= 0)
            {
                return GetEmptyTable();
            }
            var result = new StringBuilder();
            result.Append(GetTopBorderOfTheHedear());
            result.Append(GetHeaderRowOfTheTable(dataSets));
            result.Append(GetDataRowsWithSeperator(dataSets));
            result.Append(GetBottonBorderOfTableFooter());
            return result.ToString();
        }

        private string GetEmptyTable()
        {
            var textEmptyTable = "║ Table '" + tableName + "' is empty or does not exist ║";
            var result = "╔" + Repeat('═', textEmptyTable.Length - 2) + "╗\n";
            result += textEmptyTable + "\n";
            result += "╚" + Repeat('═', textEmptyTable.Length - 2) + "╝\n";
            return result;
        }

        private string GetTopBorderOfTheHedear()
        {
            return GetBorderLine(new HeaderTopBorderProperties());
        }
        private string GetHeaderRowOfTheTable(IList<IDataSet> dataSets)
        {
            var columnNames = dataSets[0].GetColumnNames();
            return GetDataRow(columnNames);
        }
        private string GetRowSeperator()
        {
            return GetBorderLine(new RowSeperatorProperties());
        }

        private string GetDataRowsWithSeperator(IList<IDataSet> dataSets)
        {
            var rowsCount = dataSets.Count;
            var result = new StringBuilder();
            for (var row = 0; row < rowsCount; row++)
            {
                var values = dataSets[row].GetValues();
                result.Append(GetRowSeperator());
                result.Append(GetDataRow(values.Select(v => v.ToString()).ToList()));
            }

            return result.ToString();
        }

        private string GetDataRow(IList<string> values)
        {
            var result = new StringBuilder();
            result.Append("║");
            for (var column = 0; column < _numberOfColumns; column++)
            {
                result.Append(PadTheFieldWithSpaces(values[column].ToString()));
                result.Append("║");

            }
            result.Append("\n");
            return result.ToString();
        }

        private string PadTheFieldWithSpaces(string value)
        {
            //Pad the field with required number of white space characters to make all the fields uniform in size
            return Stuff(_defaultColumnValue, (_maximumColumnSize - value.Length) / 2, value);

        }

        private string GetDefaultColumnValue()
        {
            return new String(' ', _maximumColumnSize);
        }

        private string GetBottonBorderOfTableFooter()
        {
            return GetBorderLine(new FooterBottomBorderProperties());
        }

        private string GetBorderLine(BorderLineProperties borderLineProperties)
        {
            var result = new StringBuilder();
            result.Append(borderLineProperties.BeginingCharacter);
            AppendColumnSeperators(borderLineProperties, result);
            AppendContinuationCharacters(borderLineProperties, result);
            result.Append(borderLineProperties.TerminatingCharacter);
            return result.ToString();

        }

        private void AppendContinuationCharacters(BorderLineProperties borderLineProperties, StringBuilder result)
        {
            for (var i = 0; i < _maximumColumnSize; i++)
            {
                result.Append(borderLineProperties.ContinuationCharacter);
            }
        }

        private void AppendColumnSeperators(BorderLineProperties borderLineProperties, StringBuilder result)
        {
            for (var j = 1; j < _numberOfColumns; j++)
            {
                AppendContinuationCharacters(borderLineProperties, result);
                result.Append(borderLineProperties.ColumnSeperator);
            }
        }

        private int GetColumnCount(IList<IDataSet> dataSets)
        {
            var result = 0;
            if (dataSets.Count > 0)
                return dataSets[0].GetColumnNames().Count;
            return result;
        }

        private int GetMaximumColumnSizeForDisplay(IList<IDataSet> dataSets)
        {
            var maxColumnSize = GetMaxColumnSizeFromDataSet(dataSets);

            if (maxColumnSize % 2 == 0)
                maxColumnSize += 2;
            else
                maxColumnSize += 3;
            return maxColumnSize;
        }

        private int GetMaxColumnSizeFromDataSet(IList<IDataSet> dataSets)
        {
            var maxLength = 0;
            if (dataSets.Count > 0)
            {
                var columnNames = dataSets[0].GetColumnNames();
                foreach (var columnName in columnNames)
                    if (columnName.Length > maxLength)
                        maxLength = columnName.Length;
                foreach (var dataSet in dataSets)
                {
                    var values = dataSet.GetValues();
                    foreach (var value in values)
                    {
                        if (value.ToString().Length > maxLength)
                            maxLength = value.ToString().Length;
                    }
                }
            }
            return maxLength;
        }

        private string Stuff(string input, int startIndex, string replacement)
        {
            return input.Remove(startIndex, replacement.Length).Insert(startIndex, replacement);
        }

        private class BorderLineProperties
        {
            internal virtual string BeginingCharacter { get; set; }
            internal virtual string ContinuationCharacter { get; set; }
            internal virtual string ColumnSeperator { get; set; }
            internal virtual string TerminatingCharacter { get; set; }
        }

        private string Repeat(char letter,int times)
        {
            return new string(letter, times);
        }

        private class HeaderTopBorderProperties : BorderLineProperties
        {
            internal HeaderTopBorderProperties()
            {
                BeginingCharacter = "╔";
                ContinuationCharacter = "═";
                ColumnSeperator = "╦";
                TerminatingCharacter = "╗\n";
            }
        }

        private class FooterBottomBorderProperties : BorderLineProperties
        {
            internal FooterBottomBorderProperties()
            {
                BeginingCharacter = "╚";
                ContinuationCharacter = "═";
                ColumnSeperator = "╩";
                TerminatingCharacter = "╝\n";
            }
        }

        private class RowSeperatorProperties : BorderLineProperties
        {
            internal RowSeperatorProperties()
            {
                BeginingCharacter = "╠";
                ContinuationCharacter = "═";
                ColumnSeperator = "╬";
                TerminatingCharacter = "╣\n";
            }
        }
    }
}