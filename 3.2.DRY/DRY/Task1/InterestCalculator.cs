﻿using System;
using System.Globalization;
using DRY.Task1.ThirdParty;

namespace DRY.Task1
{
    public class InterestCalculator
    {
        private const int SeniorityAge = 60;
        private const double RegularInterestRate = 4.5d;
        private const double InterestRateForSeniors = 5.5d;
        private const int BonusEligibilityAge = 13;

        public decimal CalculateInterest(AccountDetails accountDetails)
        {
            if (IsAccountEligibleForInterestBonus(accountDetails))
            {
                return Interest(accountDetails);
            }

            return 0;
        }

        private bool IsAccountEligibleForInterestBonus(AccountDetails accountDetails)
        {
            var ageAtTheTimeOfOpeningAccount = DurationBetweenDatesInYears(accountDetails.Birth, accountDetails.StartDate);
            return ageAtTheTimeOfOpeningAccount > BonusEligibilityAge;
        }

        private int DurationBetweenDatesInYears(DateTime from, DateTime to)
        {
            Calendar calendar = new GregorianCalendar();
            int diffYear = calendar.GetYear(to) - calendar.GetYear(from);
            if (calendar.GetDayOfYear(to) < calendar.GetDayOfYear(from))
                return diffYear - 1;
            return diffYear;
        }

        private decimal Interest(AccountDetails accountDetails)
        {
            double interest = 0;
            if (accountDetails.Age >= SeniorityAge)
            {
                interest = GetInterestForRegularPerson(accountDetails);
            }
            else
            {
                interest = GetInterestForSeniorPerson(accountDetails);
            }


            return (decimal)interest;
        }

        private double GetInterestForRegularPerson(AccountDetails accountDetails)
        {
            return CalculateInterest((double)accountDetails.Balance, DurationSinceStartDateInYears(accountDetails.StartDate), RegularInterestRate);
        }
        private double GetInterestForSeniorPerson(AccountDetails accountDetails)
        {
            return CalculateInterest((double)accountDetails.Balance, DurationSinceStartDateInYears(accountDetails.StartDate), InterestRateForSeniors);
           
        }
        
        private double CalculateInterest(double principalAmount, double durationInYears, double annualInterestRate)
        {
            return (principalAmount * durationInYears * annualInterestRate) / 100;
        }

        private double DurationSinceStartDateInYears(DateTime startDate)
        {
            var endDate = DateTime.Now;
            return DurationBetweenDatesInYears(startDate, endDate);
        }


    }
}
