﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ErrorHandling.Task1
{
    public class RuntimeException : Exception
    {
        public RuntimeException(ErrorCode errorCode)
        {
            this.ErrorCode = errorCode;
        }

        public RuntimeException()
        {

        }
        public RuntimeException(string message)
            : base(message)
        {
        }

        public RuntimeException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public ErrorCode ErrorCode { get; }
        public string ErrorMessage { get; }
    }
}
