using System;
using System.Collections.Generic;
using ErrorHandling.Task1.ThirdParty;

namespace ErrorHandling.Task1
{
    public class UserReportBuilder
    {

        private IUserDao userDao;

        public double GetUserTotalOrderAmount(string userId)
        {
            var user = GetUser(userId);
            var orders = GetOrders(user);
            var sum = CalculateTotalAmountOfSubmittedOrders(orders);
            return sum;
        }

        private IUser GetUser(string userId)
        {
            var user = userDao.GetUser(userId);
            if (user == null)
            {
                throw new RuntimeException(ErrorCode.UserNotFound);
            }

            return user;
        }

        private static IList<IOrder> GetOrders(IUser user)
        {
            var orders = user.GetAllOrders();
            if (orders.Count == 0)
            {
                throw new RuntimeException(ErrorCode.NoOrderFound);
            }
            return orders;
        }


        private double CalculateTotalAmountOfSubmittedOrders(IList<IOrder> orders)
        {
            

            double sum = 0.0;
            foreach (IOrder order in orders)
            {
                if (order.IsSubmitted())
                {
                    double total = order.Total();
                    if (total < 0)
                        throw new RuntimeException(ErrorCode.InvalidOrderAmount);
                    sum += total;
                }
            }

            return sum;
        }


        public IUserDao GetUserDao()
        {
            if(userDao == null)
            {
                throw new ArgumentNullException("userDao cannot be null");
            }
            return userDao;
        }

        public void SetUserDao(IUserDao userDao)
        {
            this.userDao = userDao ;
        }

       
    }
}
