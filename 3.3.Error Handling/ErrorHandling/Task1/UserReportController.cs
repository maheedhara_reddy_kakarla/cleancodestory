using ErrorHandling.Task1.ThirdParty;

namespace ErrorHandling.Task1
{
    public class UserReportController
    {

        private UserReportBuilder userReportBuilder;

        public string GetUserTotalOrderAmountView(string userId, IModel model)
        {
            string totalMessage;
            var responseMessage = "userTotal";
            try
            {
                totalMessage = GetUserTotalMessage(userId);
            }
            catch (RuntimeException e)
            {
                totalMessage= ErrorMessages.GetMessageBy(e.ErrorCode);
            }
            catch (System.Exception)
            {
                totalMessage = ErrorMessages.GetMessageBy(ErrorCode.GenericError);
                return totalMessage;
            }

            model.AddAttribute("userTotalMessage", totalMessage);
            return responseMessage;

        }

        private string GetUserTotalMessage(string userId)
        {
            double amount = userReportBuilder.GetUserTotalOrderAmount(userId);
            return "User Total: " + amount + "$";
        }


        public UserReportBuilder GetUserReportBuilder()
        {
            return userReportBuilder;
        }

        public void SetUserReportBuilder(UserReportBuilder userReportBuilder)
        {
            this.userReportBuilder = userReportBuilder;
        }
    }
}
