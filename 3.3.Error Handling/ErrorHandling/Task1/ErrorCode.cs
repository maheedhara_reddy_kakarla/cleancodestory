﻿namespace ErrorHandling.Task1
{
    public enum ErrorCode
    {
        GenericError = 0,
        UserNotFound = 1,
        NoOrderFound = 2,
        InvalidOrderAmount = 3,
        
    }
}