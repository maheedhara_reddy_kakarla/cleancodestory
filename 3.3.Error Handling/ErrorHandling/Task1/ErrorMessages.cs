﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ErrorHandling.Task1
{
    public class ErrorMessages
    {
        private static readonly Dictionary<ErrorCode, string> _errorCodesWithMessages = new Dictionary<ErrorCode, string>();


        static ErrorMessages()
        {
            _errorCodesWithMessages.Add(ErrorCode.UserNotFound, "WARNING: User ID doesn't exist.");
            _errorCodesWithMessages.Add(ErrorCode.NoOrderFound, "WARNING: User have no submitted orders.");
            _errorCodesWithMessages.Add(ErrorCode.InvalidOrderAmount, "ERROR: Wrong order amount.");
            _errorCodesWithMessages.Add(ErrorCode.GenericError, "technicalError");
        }
            

        public static string GetMessageBy(ErrorCode errorCode)
        {
           return _errorCodesWithMessages.GetValueOrDefault(errorCode);
        }
    }
}
